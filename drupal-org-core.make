core = 8.x
api = 2
projects[drupal][type] = core
projects[drupal][download][type] = git
projects[drupal][download][url] = https://git.drupal.org/project/drupal.git
projects[drupal][download][branch] = 8.7.x
projects[drupal][download][tag] = 8.7.5